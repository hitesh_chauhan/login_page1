
//
//  ViewController.m
//  loginPage
//
//  Created by Click Labs 107 on 10/8/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    UITextField*nameField;
    UITextField*lastName;
    UITextField*emailField;
    UITextField*numField;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_iphone5.png"]]];

    [self textFields];
    [self buttons];
    [self labels];
    [self image];
    [self labelandbutton];

    
}

-(void)labelandbutton{
    UILabel*blueLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 460, 70)];
    [blueLabel setText:@"                     EDIT PROFILE"];
    [blueLabel setTextColor:[UIColor whiteColor]];
 
    [blueLabel setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    [self.view addSubview:blueLabel];
    
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(14, 20, 25, 35)];
    
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    [self.view addSubview:backButton];
    
    
    
    
}

-(void)image{
    UIImageView*faceView=[[UIImageView alloc]initWithFrame:CGRectMake(94, 85, 113, 113)];
    [faceView setImage:[UIImage imageNamed:@"Example.png"]];
    [self.view addSubview:faceView];
    
}

-(void)labels{
    UILabel*textLabel=[[UILabel alloc]initWithFrame:CGRectMake(18, 196, 320, 40)];
    [textLabel setText:@"PERSONAL INFO"];
    [textLabel setTextColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    [self.view addSubview:textLabel];
    
    UIView*lineView=[[UIView alloc]initWithFrame:CGRectMake(160, 215, 300, 2)];
    [lineView setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    
    [self.view addSubview:lineView];
}
-(void)textFields{
    nameField=[[UITextField alloc]initWithFrame:CGRectMake(18, 246, 294, 40)];
    nameField.placeholder=@" STEVE";
    nameField.keyboardType=UIKeyboardTypeAlphabet;
    nameField .layer.borderWidth=1.50f;
    nameField.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:nameField];


    lastName=[[UITextField alloc]initWithFrame:CGRectMake(18, 296, 294, 40)];
    lastName.placeholder=@" STEPHEN";
    lastName.keyboardType=UIKeyboardTypeAlphabet;
    lastName .layer.borderWidth=1.50f;
    lastName.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:lastName];
    
    
    
    emailField=[[UITextField alloc]initWithFrame:CGRectMake(18, 346, 294, 40)];
    emailField.placeholder=@" STEVE.STEPHEN@YAHOO.COM";
    emailField.keyboardType=UIKeyboardTypeEmailAddress;
    emailField .layer.borderWidth=1.50f;
    emailField.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:emailField];
    
    
    
    numField=[[UITextField alloc]initWithFrame:CGRectMake(18, 396, 294, 40)];
    numField.placeholder=@" +1-444-242-6565";
    numField.keyboardType=UIKeyboardTypeNumberPad;
    numField .layer.borderWidth=1.50f;
    numField.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:numField];


}


-(void)buttons{
    
    UIButton*doneButton=[[UIButton alloc]initWithFrame:CGRectMake(18, 460, 294, 40)];
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
      [doneButton setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    
    [self.view addSubview:doneButton];
    
    
    UIButton*changeButton=[[UIButton alloc]initWithFrame:CGRectMake(18, 500, 294, 40)];
    [changeButton setTitle:@" CHANGE PASSWORD" forState:UIControlStateNormal];
    
    [changeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.view addSubview:changeButton];
    
    
    
    
    
}




- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];

    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
