//
//  main.m
//  loginPage
//
//  Created by Click Labs 107 on 10/8/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
